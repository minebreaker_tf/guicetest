package sample.provider;

import com.google.inject.Inject;

public class ProviderPrintServiceImpl implements ProviderPrintService {

    private final ProviderGreetService greetService;

    @Inject
    public ProviderPrintServiceImpl(ProviderGreetService greetService) {
        this.greetService = greetService;
    }

    @Override
    public void cout() {
        System.out.println(greetService.greet());
    }

}
