package sample.provider;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;

public class ProviderModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ProviderPrintService.class).toProvider(ProviderPrintServiceProvider.class);
    }

    @Provides
    @Named("Greeter")
    ProviderGreetService provideGreetService() {
        return new ProviderGreetServiceImpl();
    }

}
