package sample.provider;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;


public class ProviderPrintServiceProvider implements Provider<ProviderPrintService> {

    private final ProviderGreetService greeter;

    @Inject
    public ProviderPrintServiceProvider(@Named("Greeter") ProviderGreetService greeter) {
        this.greeter = greeter;
    }

    @Override
    public ProviderPrintService get() {
        return new ProviderPrintServiceImpl(greeter);
    }

}
