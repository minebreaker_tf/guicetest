package sample.provider;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class ProviderMain {

    // https://github.com/google/guice/wiki/ProvidesMethods
    // https://github.com/google/guice/wiki/ProviderBindings

    public static void main(String[] args) {

        Injector injector = Guice.createInjector(new ProviderModule());
        injector.getInstance(ProviderPrintService.class).cout();

    }

}
