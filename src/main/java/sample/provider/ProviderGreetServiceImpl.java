package sample.provider;

public class ProviderGreetServiceImpl implements ProviderGreetService {

    @Override
    public String greet() {
        return "hello, world!";
    }

}
