package sample.named;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

public class NamedModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(NamedPrintService.class).to(NamedPrintServiceImpl.class);
        bind(NamedGreetService.class).annotatedWith(Names.named("English")).to(NamedEnglishGreetServiceImpl.class);
        bind(NamedGreetService.class).annotatedWith(Japanese.class).to(NamedJapaneseGreetServiceImpl.class);
    }

}
