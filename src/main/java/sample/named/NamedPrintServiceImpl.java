package sample.named;

import com.google.inject.Inject;
import com.google.inject.name.Named;

public class NamedPrintServiceImpl implements NamedPrintService {

    private final NamedGreetService englishGreetService;
    private final NamedGreetService japaneseGreetService;

    @Inject
    public NamedPrintServiceImpl(@Named("English") NamedGreetService englishGreetService, @Japanese NamedGreetService japaneseGreetService) {
        this.englishGreetService = englishGreetService;
        this.japaneseGreetService = japaneseGreetService;
    }

    @Override
    public void cout() {
        System.out.println(englishGreetService.greet());
        System.out.println(japaneseGreetService.greet());
    }

}
