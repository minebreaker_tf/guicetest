package sample.named;

public interface NamedGreetService {

    public String greet();

}
