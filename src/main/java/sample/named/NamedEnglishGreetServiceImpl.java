package sample.named;

public class NamedEnglishGreetServiceImpl implements NamedGreetService {

    @Override
    public String greet() {
        return "hello, world!";
    }

}
