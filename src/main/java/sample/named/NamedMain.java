package sample.named;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class NamedMain {

    // https://github.com/google/guice/wiki/BindingAnnotations

    public static void main(String[] args) {

        Injector injector = Guice.createInjector(new NamedModule());
        injector.getInstance(NamedPrintService.class).cout();

    }

}
