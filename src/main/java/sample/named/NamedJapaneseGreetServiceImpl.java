package sample.named;

public class NamedJapaneseGreetServiceImpl implements NamedGreetService {

    @Override
    public String greet() {
        return "こんにちは、世界!";
    }

}
