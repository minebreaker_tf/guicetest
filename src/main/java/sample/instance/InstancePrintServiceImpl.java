package sample.instance;

import com.google.inject.Inject;

public class InstancePrintServiceImpl implements InstancePrintService {

    private final InstanceGreetService greetService;

    @Inject
    public InstancePrintServiceImpl(InstanceGreetService greetService) {
        this.greetService = greetService;
    }

    @Override
    public void cout() {
        System.out.println(greetService.greet());
    }

}
