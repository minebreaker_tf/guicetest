package sample.instance;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class InstanceMain {

    // https://github.com/google/guice/wiki/InstanceBindings

    public static void main(String[] args) {

        Injector injector = Guice.createInjector(new InstanceModule());
        injector.getInstance(InstancePrintService.class).cout();

    }

}
