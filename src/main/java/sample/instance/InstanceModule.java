package sample.instance;

import com.google.inject.AbstractModule;

public class InstanceModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(InstancePrintService.class).to(InstancePrintServiceImpl.class);
        bind(InstanceGreetService.class).toInstance(new InstanceGreetServiceImpl());
    }

}
