package sample.instance;

public class InstanceGreetServiceImpl implements InstanceGreetService {

    @Override
    public String greet() {
        return "hello, world!";
    }

}
