package sample.linked;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class LinkedMain {

    // https://github.com/google/guice/wiki/LinkedBindings

    public static void main(String[] args) {

        Injector injector = Guice.createInjector(new LinkedModule());
        injector.getInstance(LinkedPrintService.class).cout();

    }

}
