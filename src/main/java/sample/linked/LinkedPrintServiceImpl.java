package sample.linked;

import com.google.inject.Inject;

public class LinkedPrintServiceImpl implements LinkedPrintService {

    private final LinkedGreetService greetService;

    @Inject
    public LinkedPrintServiceImpl(LinkedGreetService greetService) {
        this.greetService = greetService;
    }

    @Override
    public void cout() {
        System.out.println(greetService.greet());
    }

}
