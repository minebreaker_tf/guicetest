package sample.linked;

public class LinkedGreetServiceImpl implements LinkedGreetService {

    @Override
    public String greet() {
        return "hello, world!";
    }

}
