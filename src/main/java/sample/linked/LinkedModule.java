package sample.linked;

import com.google.inject.AbstractModule;

public class LinkedModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(LinkedPrintService.class).to(LinkedPrintServiceImpl.class);
        bind(LinkedGreetService.class).to(LinkedGreetServiceImpl.class);
    }

}
