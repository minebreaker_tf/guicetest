package sample.singleton;

public interface SingletonGreetService {

    public String greet();

}
