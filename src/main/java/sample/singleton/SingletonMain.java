package sample.singleton;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Stage;

public class SingletonMain {

    public static void main(String[] args) {

        Injector injector = Guice.createInjector(Stage.PRODUCTION, new SingletonModule());
        injector.getInstance(SingletonPrintService.class).cout();

    }

}
