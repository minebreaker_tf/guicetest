package sample.singleton;

public interface SingletonPrintService {

    public void cout();

}
