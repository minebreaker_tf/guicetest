package sample.singleton;

import com.google.inject.Singleton;

@Singleton
public class SingletonGreetServiceImpl implements SingletonGreetService {

    @Override
    public String greet() {
        return "hello, world!";
    }

}
