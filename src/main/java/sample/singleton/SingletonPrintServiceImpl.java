package sample.singleton;

import com.google.inject.Inject;

public class SingletonPrintServiceImpl implements SingletonPrintService {

    private final SingletonGreetService greetService;

    @Inject
    public SingletonPrintServiceImpl(SingletonGreetService greetService) {
        this.greetService = greetService;
    }

    @Override
    public void cout() {
        System.out.println(greetService.greet());
    }

}
