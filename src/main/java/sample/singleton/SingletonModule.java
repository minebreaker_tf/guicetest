package sample.singleton;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

public class SingletonModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(SingletonPrintService.class).to(SingletonPrintServiceImpl.class).in(Singleton.class);
        bind(SingletonGreetService.class).to(SingletonGreetServiceImpl.class);
    }

}
