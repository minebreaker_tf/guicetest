package sample.justintime;

public class JitGreetServiceImpl implements JitGreetService {

    @Override
    public String greet() {
        return "hello, world!";
    }

}
