package sample.justintime;

import com.google.inject.ImplementedBy;

@ImplementedBy(JitGreetServiceImpl.class)
public interface JitGreetService {

    public String greet();

}
