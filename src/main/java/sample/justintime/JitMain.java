package sample.justintime;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class JitMain {

    public static void main(String[] args) {

        Injector injector = Guice.createInjector();
        injector.getInstance(JitPrintService.class).cout();

    }

}
