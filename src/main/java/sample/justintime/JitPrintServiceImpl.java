package sample.justintime;

import com.google.inject.Inject;

public class JitPrintServiceImpl implements JitPrintService {

    private final JitGreetService greetService;

    @Inject
    public JitPrintServiceImpl(JitGreetService greetService) {
        this.greetService = greetService;
    }

    @Override
    public void cout() {
        System.out.println(greetService.greet());
    }

}
