package sample.justintime;

import com.google.inject.ImplementedBy;

@ImplementedBy(JitPrintServiceImpl.class)
public interface JitPrintService {

    public void cout();

}
