package sample.scope;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

public final class ScopedMain {

    @Inject
    private TestScope scope;

    public static void main(String[] args) throws Exception {

        Injector injector = Guice.createInjector(new ScopedModule());

        TestScope scope = injector.getInstance(TestScope.class);

        ScopedGreetService scoped = scope.inScope(() -> {
            ScopedGreetService s1 = injector.getInstance(ScopedGreetService.class);
            ScopedGreetService s2 = injector.getInstance(ScopedGreetService.class);
            System.out.println(s1 == s2);  // true
            return s1;
        });
        ScopedGreetService another = scope.inScope(() -> {
            return injector.getInstance(ScopedGreetService.class);
        });
        System.out.println(scoped == another);  // false
    }

}
