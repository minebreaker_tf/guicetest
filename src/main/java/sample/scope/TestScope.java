package sample.scope;

import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.Scope;
import com.google.inject.TypeLiteral;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

// Not thread safe!
public final class TestScope implements Scope {

    private Map<TypeLiteral<?>, Object> objects;

    public <T> T inScope(Callable<T> runner) throws Exception {
        objects = new HashMap<>();
        T obj = runner.call();
        objects = null;
        return obj;
    }

    @Override
    public <T> Provider<T> scope(Key<T> key, Provider<T> unscoped) {
        return () -> {
            if (objects.containsKey(key.getTypeLiteral())) {
                return (T) objects.get(key.getTypeLiteral());
            } else {
                T obj = unscoped.get();
                objects.put(key.getTypeLiteral(), obj);
                return obj;
            }
        };
    }

}
