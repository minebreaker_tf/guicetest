package sample.scope;

import com.google.inject.AbstractModule;

public final class ScopedModule extends AbstractModule {
    @Override
    protected void configure() {

        TestScope scope = new TestScope();

        bindScope(TestScoped.class, scope);
        bind(TestScope.class).toInstance(scope);
        bind(ScopedGreetService.class).to(ScopedGreetServiceImpl.class).in(scope);
    }
}
