package sample.scope;

public final class ScopedGreetServiceImpl implements ScopedGreetService {
    @Override
    public void print() {
        System.out.println("hello, scope!");
    }
}
