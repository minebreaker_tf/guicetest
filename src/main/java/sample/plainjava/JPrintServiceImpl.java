package sample.plainjava;

public class JPrintServiceImpl implements JPrintService {

    private final JGreetService JGreetService;

    public JPrintServiceImpl(JGreetService JGreetService) {
        this.JGreetService = JGreetService;
    }

    @Override
    public void cout() {
        System.out.println(JGreetService.greet());
    }

}
